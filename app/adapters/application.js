import ActiveModelAdapter from 'active-model-adapter'

export default DS.JSONAPIAdapter.extend({
  namespace : 'api/v2'
});
